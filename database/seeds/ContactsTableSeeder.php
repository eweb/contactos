<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker\Factory::create();
        $limit = 6;

        for ($i = 0; $i < $limit; $i++) {
       		DB::table('contacts')->insert([ //,
	              	'user_id' => App\User::all()->random()->id,
	                'name' => $faker->firstname,
                    'email' => $faker->unique()->email,
	                'file' => $faker->word.".pdf",
	            ]);
        }
    }
}
