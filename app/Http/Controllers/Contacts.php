<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Storage;
use Session;

class Contacts extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objContact= new \App\Contact();
        $user= \Auth::user();
        $data=$user->contacts->get->all();
        dd($data);
        return view('home')->with('contacts',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrData = array('name' =>'',
             'email'=>'',
             'file'=>'',
             'id'=>'',
             'title'=>'Crear Contacto',
             'route'=>'contacts/');
        Session::forget('msj');
        Session::forget('nomsj');
        //dd($arrData);
        return view('contacts.create')->with(['contact'=>$arrData]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function saveImg($img){;
        $name_file=(time().'_'.str_replace(' ', '_', $img->getClientOriginalName()));
        if(\Storage::disk('imgContacts')->put($name_file,\File::get($img))){
            return $name_file;
        }else{
            false;
        }
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email'
            //,'contactfile'=>'required'
            ]);

        //llamada a Objetos
        $user            =\Auth::user();
        $contacts        =new \App\Contact();
        
        //carga de valores
        $contacts->name  =$request->name;
        $contacts->email =$request->email;

        if(!empty($request->contactfile)){
            $img=$request->contactfile;
            $nameFile=$this->saveImg($img);
            if($nameFile!=false){
                $contacts->file=$nameFile;
            }
        }
        if ($user->contacts()->save($contacts)->save()) {
            //Variable de Session
            return back()->with('msj','Datos Guardados con Exito');
        }else{
            return back()->with('nomsj','Error en la Base de Datos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objContact= new \App\Contact();
        $data=$objContact::find($id);
        $arrData = array('name' =>$data->name,
             'email'=>$data->email,
             'file'=>$data->file,
             'id'=>$data->id,
             'title'=>'Editar Contactos',
             'route'=>'contacts/');
        Session::forget('msj');
        Session::forget('nomsj');
        //dd($arrData);
        return view('contacts.create')->with(['contact'=>$arrData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request);
        $objContact  = new \App\Contact();
        $data        =$objContact::find($id);
        $data->email =$request->email;
        $data->name  =$request->name;

        if (!empty($request->file)) {
            if(!empty($data->file)){
                $delete=$this->delete($id);
            }
            $nameImg=$this->saveImg($request->file);
            if($nameImg!=false){
                $data->file=$nameImg;
            }
        }
        if ($data->save()) {
            $arrData = array('name' =>$data->name,
                 'email'=>$data->email,
                 'file'=>$data->file,
                 'id'=>$data->id,
                 'title'=>'Editar Contactos',
                 'route'=>'contacts/');
        return view('create.contacts')->with(['contact'=>$arrData]);
        }else{
            return view('errors.503');
        }
    }



    public function deleteImg($id){
        $objContact= new \App\Contact();
        $data=$objContact::find($id);
        $img=$data->file;
        $delete=\Storage::disk('imgContacts')->delete($img);
        if($delete){
            $data->file=null;
            return $data->save();
        }else{
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
