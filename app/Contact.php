<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //MODELO DE CONTACTOS
    protected $fillable = [
        "user_id","name","email"
    ];

    //Relacion de pertenencia
    public function user(){
    	return $this->belongsTo('\App\User');
    }

}