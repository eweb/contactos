  <table class="table">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Email</th>
        <th> </th>
      </tr>
    </thead>
    <tbody>
      @if(!empty($contacts))
        @foreach($contacts as $cont)
          <tr>
            <td>{{$cont->name}}</td>
            <td>{{$cont->email}}</td>
            <td>
              <a href="contacts/{{$cont->id}}/edit">Editar</a>
              <a href="{{route('contacts.destroy',array($cont->id)) }}" data-method="delete" rel="nofollow" data-confirm="Desea Eliminar?">Eliminar</a>
            </td>
          </tr>
        @endforeach
      @else
        <td colspan="3">No hay contactos que mostrar</td>
      @endif

    </tbody>
  </table>