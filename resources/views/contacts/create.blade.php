{{-- Informar a los Usuarios de los errores --}}

@if(!empty($errors))
  <ul>
    @foreach($errors->all() AS $error)
      <li>{{$error}}</li>
    @endforeach
  </ul>
@endif
{{--VALIDACIONES--}}
@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">{{$contact['title']}}}</div>
          <div class="panel-body">
            @if(session()->has('msj'))
            <div class="alert alert-success">{{session('msj')}}</div>
            @elseif(session()->has('nomsj'))
            <div class="alert alert-success">{{session('msj')}}</div>
            @endif

            <form role="form" action="{{url($contact['route'])}}" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="name">Nombres</label>
                <input name="name" type="text" class="form-control" id="name" placeholder="Pedro Eduardo Perez" value="{{$contact['name']}}">
              </div>  

              <div class="form-group">
                <label for="email">Email</label>
                <input name="email" type="email" class="form-control" id="Email1" placeholder="pedroperez@ejemplo.com" value="{{$contact['email']}}">
              </div>
              @if($errors->has('name'))
              <span style="color:red">{{$errors->first('email')}}</span>
              @endif
              <div class="form-group">
                <label for="contactfile">Archivo</label>
                <input name="contactfile" type="file" id="contactfile">
                <p class="help-block">Puede subir su imagen aqui.</p>
              </div>
              @if(!empty($contact['file']))
                <div style="align-content:center">
                  <img src="{{url('imgContacts/'.$contact['file'])}}" alt="">
                </div>
              @endif

              @if(!empty($contact['id']))
                <input type="hidden" name="_method" value="PUT">
              @endif

              <button type="submit" class="btn btn-default">Enviar</button>
              {{csrf_field()}}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

